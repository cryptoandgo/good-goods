
# Summary

Good Goods uses blockchain technology to verify the ethical nature of goods sourced from either countries or companies. The system uses **smart contracts** interacting with APIs. The main point of contact is a Smart Contract running on the **Ethereum network** using **Chainlink oracles** to verify data against a **remote API**.

## What can be verified?

The first version of the system integrates with the **United States Department of Labor** to verify if goods were produced using slave and/or child labour. The current API allows for passing the country of origin and the goods in question. This will return a simple boolean indicating if the goods are, well, good.

Future versions will integrate into other data sources and allow for verifing if companies are knowingly working with problem suppliers or have sourced any parts through questionable supply chains. Various response types in future will also be integrated to allow for more nuanced decison making.

## Uses for the system

The most basic use would be to finalise a contract and make payment to a supplier only if the goods are verified to be ethical.
