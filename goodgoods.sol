pragma solidity ^0.6.0;

import "github.com/smartcontractkit/chainlink/evm-contracts/src/v0.6/ChainlinkClient.sol";

contract GoodGoods is ChainlinkClient
{
    // internal
    uint256 public isGood;

    // chainlink oracles
    address private oracle;
    bytes32 private jobId;
    uint256 private fee;

    // setup contract
    constructor()
    public
    {
        setPublicChainlinkToken();

        oracle = 0x0;
        jobId = "";
        fee = 0.1 * 10 ** 18;
    }

    // check status of goods
    function checkIfAllGood(string _origin, string _goods)
    public
    returns (bytes32 requestId)
    {
        Chainlink.Request memory request = buildChainlinkRequest(jobId, address(this), this.fulfill.selector);
        request.add("get", string(abi.encodePacked("https://example.com/api/isgood?origin=", _origin, "&goods=", _goods)));
        request.add("path", "status");
        sendChainlinkRequestTo(oracle, request, fee);
    }

    // save goods status
    function fulfill(bytes32 _requestId, uint256 _status)
    public
    recordChainlinkFulfillment(_requestId)
    {
        isGood = _status;
    }

}
